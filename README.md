# Nginx with fancyindex module

---

Official NGINX Dockerfiles: https://github.com/nginxinc/docker-nginx

FancyIndex: https://github.com/aperezdc/ngx-fancyindex
